Hola, gracias por visitar este proyecto. Estos son los archivos necesarios para implementar Traefik v2.1.3 junto con Docker usando Terraform sobre DigitalOcean. 

Puedes ver el tutorial sobre este proyecto en el siguiente enlace: https://www.cduser.com/como-implementar-traefik-2-1-2-con-docker-usando-terraform-en-digitalocean/

Puedes hacerme cualquier pregunta al respecto en los comentarios del blog, me puedes encontrar en el Slack de SysArmy: https://nerdearla.slack.com o también en Twitter: https://twitter.com/hectorivand
