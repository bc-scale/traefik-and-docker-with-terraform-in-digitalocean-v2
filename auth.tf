# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "https://cloud.digitalocean.com/account/api/tokens"
}
